package com.sp.user;

import com.sp.user.dto.UserDTO;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@RestController
public class UserRestPoint {

    private final static Map<Long, UserDTO> USERS = new HashMap<>();

    public UserRestPoint() {
        USERS.put(1L, new UserDTO(1L, "Mateusz", "Gocz", "mg@mail.com", "Wrocław, Tęczowa 85/36"));
        USERS.put(2L, new UserDTO(2L, "Paulina", "Gocz", "pg@mail.com", "Wrocław, Tęczowa 85/36"));
        USERS.put(3L, new UserDTO(3L, "Wojtek", "Kusienicki", "wk@mail.com", "Wrocław, Tęczowa 85/36"));
        USERS.put(4L, new UserDTO(4L, "Mateusz", "Smolka", "ms@mail.com", "Wrocław, Tęczowa 85/36"));
        USERS.put(5L, new UserDTO(5L));
    }

    @GetMapping(path = "user/{id}")
    public UserDTO getUserById(@PathVariable("id") long id) {
        return USERS.get(id);
    }

    /*@PostMapping(path = "user/{id}")
    public UserDTO updateUsername(@PathVariable("id") long id, @RequestParam("username") String username) {
        UserDTO user = USERS.get(id);
        user.setUsername(username);
        return user;
    }*/

    @DeleteMapping(path = "user/{id}")
    public void deleteUserById(@PathVariable("id") long id) {
        USERS.remove(id);
    }

    @GetMapping(path = "users")
    public Collection<UserDTO> getAllUsers() {
        return USERS.values();
    }
}
