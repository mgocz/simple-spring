package com.sp.user.dto;

public class UserDTO {
    private final Long id;
    private UserPersonalDataDTO personalData;
    private String address;

    public UserDTO(Long id) {
        this.id = id;
    }

    public UserDTO(Long id, String firstName, String lastName, String email, String address) {
        this.id = id;
        this.personalData = new UserPersonalDataDTO(firstName, lastName, email);
        this.address = address;
    }

    public Long getId() {
        return id;
    }

    public UserPersonalDataDTO getPersonalData() {
        return personalData;
    }

    public String getAddress() {
        return address;
    }
}
